FROM alpine

ARG TERRAFORM_VERSION="1.4.2"
ARG ANSIBLE_VERSION="5.6.0"

LABEL maintainer="Tomas Sapak <139890@muni.cz>"
LABEL terraform_version=${TERRAFORM_VERSION}
LABEL ansible_version=${ANSIBLE_VERSION}

ENV DEBIAN_FRONTEND=noninteractive
ENV TERRAFORM_VERSION=${TERRAFORM_VERSION}
ENV ANSIBLE_VERSION=${ANSIBLE_VERSION}

RUN export CRYPTOGRAPHY_DONT_BUILD_RUST=1\
    && apk add --no-cache --virtual build-dependencies python3-dev libffi-dev openssl-dev build-base unzip curl \
    && apk add openssh python3 git py3-pip jq yq \
    && pip3 install --upgrade pip \
    && pip3 install ansible==${ANSIBLE_VERSION} cryptography jinja2-cli passlib python-hcl2 netaddr \
    && ansible-galaxy collection install community.docker \
    && curl -LO https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
    && unzip '*.zip' -d /usr/local/bin \
    && rm *.zip \
    && apk del build-dependencies \
    && rm -rf /var/cache/apk/*

CMD    ["/bin/bash"]
